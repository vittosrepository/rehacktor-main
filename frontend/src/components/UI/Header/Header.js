import Video from "./../../../Assets/video.mp4";
import classes from './Header.module.css';

export default function Header(){
      return(
      <header>
       <div className={classes.overlay}></div>
    
       <video playsInline autoPlay muted loop >
         <source src={Video} type="video/mp4" />
       </video>
    
       <div className={classes.container + " h-100"} >
         <div className="d-flex h-100 text-center align-items-center">
           <div className="w-100 text-white">
             <h1 className="display-3">ReHacktor</h1>
             <p className="lead mb-0">Explore Rehacktor, thw only site allows you to discover new games and share experience with friends!</p>
           </div>
         </div>
       </div>
     </header>
      );      
}