import classes from "./Navbar.module.css";
import { Link } from "react-router-dom"; /* Con il tag <Link> non si usa l'href come con il tag <a> ma si usa il to="" */
import { AuthContext } from "../../../Contexts/Auth";
import { useState, useContext } from "react";
import Modal from "../Modal/Modal";

export default function Navbar() {

  const { user, logout } = useContext(AuthContext);


  const [modal, setModal] = useState(false);

  const closeModal = () => setModal(false);

      return(
            <nav 
              className={
                  "navbar navbar-expand navbar-dark shadow fixed-top " + classes.navbar
              }
            >
              <div className="container-fluid">
                <Link className="text-decoration-none font-exan text-main" to="/">
                <i className="fab fa-react fa-spin me-3"></i>        
                ReHacktor
                </Link>
                
              <div className={classes.navLogo}></div>

              <div className="collapse navbar-collapse">
                <ul className="navbar-nav ms-auto">
                  <li className="nav-item">
                    <Link className="nav-link" to="/search/action/1">
                       Search
                    </Link>  
                  </li>

                  {!user && (
                    <li className="nav-item">
                      <Link className="nav-link" to="/sign">
                        Sign
                      </Link>  
                    </li>
                  )}
                
                {user && (
                  <>
                  {modal && (
                  <Modal
                    title="Oh no..."
                    message="Vuoi già lasciarci? Ricorda che eventuali stream saranno interrotti."
                    declineMessage="Rimani"
                    confirmMessage="Esci"
                    closeModal={closeModal} 
                    action={logout}
                  />
                  )}
                    <li className="nav-item">
                      <Link to="/profile" className="nav-link text-white">
                       <i className="fas fa-user-circle me-2"></i>
                      {user.username}
                      </Link>
                    </li>
                    <li className="nav-item">
                      <button className="btn" onClick={() => setModal(true) }>
                       <i className="fa-solid fa-arrow-right-from-bracket text-main"></i>
                      </button>
                  </li>
                </>
                )}
                
                </ul>
              </div>
              </div>              
            </nav>
      );
}