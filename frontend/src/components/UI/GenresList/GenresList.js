import classes from "./GenresList.module.css";
import { Link } from "react-router-dom";

export default function GenresList(props) {

      return (
           <div className={classes["genres-wrapper"]}>
            {props.data.map((genre) =>(
             <Link className="text-decoration-none" to={`/search/${genre.slug}/1`}>
              <button 
               key={genre.id} 
               className="btn btn-outline-info rounded-0 d-block w-100 text-start mb-2"
              >
              {genre.name}
             </button>
             </Link>
            ))}  
           </div>
      );
}