import {useParams} from "react-router-dom";
import {useState, useEffect} from "react";
import {Link} from "react-router-dom";
import Loader from "../../UI/Loader/Loader";
import { useContext } from "react";
import { ConfigContext } from "../../../Contexts/Config/index";

export default function Game() {
      let { slug } = useParams();

      let { api_urls, api_secrets } = useContext(ConfigContext);

      const [game, setGame] = useState(null); 

      useEffect(() => {
        fetch(`${api_urls.games}/api/games/${slug}?&key=${api_secrets.games}`)
             .then((response) => response.json())
             .then((data) => {
                  setGame(data);
            });
      }, []);

      // console.log(api_secrets);

      // Capire perchè quando si inserisce ${api_urls.games} ed ${api_secrets.games} le card dei giochi caricano all'infinito e non vanno -> RISOLTO mettendo il file .env nella directory frontend e riavviando react

      return ( 
          <>

      {     
            game ? (
             <div className="container-fluid pt-5 min-vh-100" 
            style={{
               background: `linear-gradient(rgba(33,33,33,1), rgba(33,33,33,0.8), rgba(33,33,33,1)), url(${game.
               background_image})`,
               backgroundSize: "cover",
               backgroundPosition: "center",
               backgroundrepeat: "no-repeat",
            }}
            >
              <div className="container"> 
                <div className="row mt-5">
                  <div className="col-12">
                    <h1>{game.name}</h1>
                    <p className="small">Developed by {game.developers[0].name} </p>
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col-12 col-md-6">
                    {game.description_raw}
                  </div>
                  <div className="col-12 col-md-6">
                    <img className="img-fluid" 
                    src={game.background_image} 
                    alt={game.name} 
                    />
                  </div>              
                </div>
               <div className="row">
                <h3>Genres</h3>
                <div className="col-3"> 
                {game.genres
                  .map((el) => (
                  <Link 
                  key={el.id} 
                  to={`/search/${el.slug}/1`} 
                  className="text-decoration-none"
                  >
                    <button className="btn btn-outline-info mx-1">{el.name}</button>
                    </Link>
                  ))}
                  </div>
               </div>

                <div className="row">
                 <div className="col-6 my-3">
                    <h3 className="my-4 text-main">Informations</h3>
                    <h5>WEBSITE</h5>
                    {game.website && /* se game.website esiste allora mi crea il button altrimenti no. è in pratica una sorta di if*/                  
                     <a href={game.website} className="text-decoration-none text-white"><i className="fas fa-share mb-4 text-primary"></i> Go to <i className="fas fa-chevron-right"></i> </a>
                    }
                    <h5>RELEASED</h5>
                    {game.released &&                   
                     <p><i className="fas fa-share mb-2 text-primary"></i> {game.released}</p>
                    }
                    <h5>PLAYTIME</h5>
                    {game.playtime &&                   
                     <p><i className="fas fa-share mb-4 text-primary"></i> {game.playtime} h</p>
                    }
                 </div>
                 <div className="col-6 my-3">
                      <h3 className="text-main"><i className="fas fa-chevron-right"></i> <a href="/" className="text-decoration-none text-main">Start Your Stream</a></h3>
                 </div>
                </div>

              </div>
            </div>
            ) : (<Loader />)}
          </>
      );
}
