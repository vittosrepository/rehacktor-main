import "./App.css";
import Footer from "./components/UI/Footer/Footer";
import Home from './components/Views/Home/Home';
import Navbar from "./components/UI/Navbar/Navbar";
import Search from "./components/Views/Search/Search";
import Game from "./components/Views/Game/Game";
import { BrowserRouter, Routes, Route } from "react-router-dom"; /* Sintassi di router-dom aggiornata alla versione 6  */
import { AuthProvider } from "./Contexts/Auth";
import { ConfigProvider } from "./Contexts/Config/index";
import Sign from "./components/Views/Sign/Sign";
import Profile from "./components/Views/Profile/Profile";


function App() {
  return ( 
    <ConfigProvider>
        <AuthProvider>
        <BrowserRouter>
          <Navbar />
        
        <Routes>
          <Route path="/game/:slug" element={<Game />} />
          <Route path="/search/:genre/:num" element={<Search />} />
          <Route path="/sign" element={<Sign />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/" element={<Home />} />
        </Routes>
        
        <Footer />
        </BrowserRouter>
      </AuthProvider>
    </ConfigProvider> 
  );
}

export default App;

