<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class UserController extends Controller
{
    public function register(Request $request) {
        
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'requiered|string|unique:users',
            'password' => 'required|min:6|confirmed', #https://laravel.com/docs/8.x/validation#rule-confirmed
        ]);
        
        if ($validato->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->messages()-toArray()
            ], 500);
        }

        User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => Hash::make($request->$password)
        ]);

        $responseMessage = "Registration Successful";

        return response()->json([
            'success' => true,
            'message' => $respondeMessage
        ], 200);
    }
}
