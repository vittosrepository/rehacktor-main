include ./.env
export

# help ## Show this help

#       echo  "Usage: make [optins] [target] ..."; \    
      
#       echo "Target:"; \

#       fgrep -h '##' Makefile | awk -F'## | : ' '{print "%40s $s\n", $$1, $$3}
#       ' | fgrep -v 'fgrep';


# freshdb ## reset db

#       echo "Reset DB"

#       cd backend && php artisan migrate:fresh && php artisan passport:install --force


dev:   ##serve for development

      echo "starting Dev Environment"

      cd frontend && npm start &

      cd backend && php artisan serve


# install ## performs initial setup

#       echo "Installing Libraries"

#       cd backend && composer install && cp .env.example .env

#       cd frontend && npm install